## THIS

This is a simple hello-word Node.js app used for pipeline exercices that can be dockerized using the Dockerfile. 

Right now there's a `.gitlab-ci.yml` that triggers the Image build and Push to dockerhub everytime there is a commit that changes the app folder. This CI uses the protected and masked variables defined in the repository for the `image name`, `repository` and `user authentication`.

The pipeline has two simple `stages`:
- `build`: pulls the image:latest from the dockerhub repository (if exists) > builds the Dockerfile as image:commit-sha using the cache of latest > and then pushes this image to the repository. Building from the cache ensures that the process is at least faster than building from scratch.
- `push`: pulls the image:commit-sha from the dockerhub repository > builds as image:latest > pushes the Image back to the repository.

This process ensures that the dockerhub repository defined for this gitlab repository will *support versioning of the images* because every commit made to the master branch will be builded and tagged with the commit-sha, and **only the last commit will be tagged as latest**.

Finally, the `build stage` will only be executed when there are changes in the `app/` folder, this by using `rules`, and the `push stage` will only execute if the Build Job has been executed, this by using `needs`. This way we ensure there isn't unnecesary builds when changing the README, .gitlab-ci.yml or the docker files. Although this isn't how Production servers may work, it improves this specific example. 

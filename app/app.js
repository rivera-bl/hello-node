const express = require('express');
const app = express();

const PORT = 8080;

app.get('/', (req, res) => res.send('hello world from gitlab-ci'));

app.listen(PORT, () => {
    console.log(`Server up at port ${PORT}`)
})



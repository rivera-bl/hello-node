# most lightweight base image of node
FROM node:14-alpine
# create the directory and uses it for the next COPY/RUN commands
WORKDIR /usr/src/app
# copy the package*.json files to the workdir folder
COPY app/package*.json ./
# npm installs the dependencies defined in the previous files
RUN npm install 
# copy the app.js to the workdir folder
COPY app/app.js ./
# expose the port used by the node app
EXPOSE 8080
# run the start script for the node app
CMD ["npm", "start"]
